// import {TabulatorFull as Tabulator} from 'tabulator-tables';

function getCount(data, prop){
    let counts = {};
    data.forEach(d => {
        if(d[prop] != '') // Removes blank data 
        {
          counts[d[prop]] = (counts[d[prop]] || 0) + 1;
        }
    })
  
    let countObj = []; 
  
    for (let i in counts) {
        countObj.push( {
            [prop]: i,
            count: counts[i], 
        })
    } 
  
    return countObj;  
  }
  
  let starHabitability = {
    A : {inner: 8.5, outter: 12.5},
    F : {inner: 1.5, outter: 2.2},
    G : {inner: 0.95, outter: 1.4},
    K : {inner: 0.38, outter: 0.56},
    M : {inner: 0.08, outter: 0.12},
  }
  
  function isHabitable(star, dist) {
    if(star == undefined || star == '') return 'Unknown';
    if(!(star == 'A' || star == 'F' || star == 'G' || star == 'K' || star == 'M')) return 'Unknown' 
  
    if(dist < starHabitability[star].outter && dist > starHabitability[star].inner){
      return 'Habitable ';
    }
    return 'Uninhabitable'; 
  }
  
  
  function resetFilters() {
    visuals.forEach(v => {
        v.data = this.masterData;
        v.updateVis(); 
    })
  
    this.table.setData(this.masterData); 
  }
  
  
  function dataFilter(selection, attr, graph){
  
    this.data = this.masterData.filter(d =>d[attr] == selection[attr])
  
    visuals.forEach(v => {
      if(!(v.config.parentElement == graph)){
        v.data = this.data;
        v.updateVis(); 
      } 
      else {
        v.data = this.masterData;
        v.updateVis();  
        this.showing = v.config.axis.xTitle + ' ' + selection[attr];
      }
    })

    this.table.setData(this.data);

    d3.select('filter')
        .join()
        .text("Showing: " + showing); 
  
    lastClicked = selection;  
  
  }