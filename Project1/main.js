d3.csv('exoplanets-1-noBlank.csv')
  .then(_data => {
  	console.log('Data loading complete.');
  	data = _data;
    
    // Data processing -> Convert string values to integers when needed  
    data.forEach(d => { 
      	d.pl_bmasse = +d.pl_bmasse
        d.pl_orbeccen = +d.pl_orbeccen
        d.pl_orbsmax = +d.pl_orbsmax
        d.pl_rade = +d.pl_rade
        d.st_mass = +d.st_mass
        d.st_rad = +d.st_rad
        d.sy_dist = +d.sy_dist
        d.sy_pnum = +d.sy_pnum
        d.sy_snum = +d.sy_snum
        d.st_spectype = d.st_spectype.charAt(0).toUpperCase()
        d.habitable = isHabitable(d.st_spectype, d.pl_orbsmax)
  	});
    
    data = data.filter(d => !(d.pl_bmasse == 0 || d.pl_rade == 0)); //Removes radius and mass of 0 
     
    masterData = data; 

    console.log(data);

    showing = 'All Data';

    d3.select('header')
      .append('filter')
      .text("Showing: " + showing);

    table = new Tabulator("#test-table", {
      height: 205, 
      data: this.data, 
      layout:"fitColumns", //fit columns to width of table (optional)
      columns:[ //Define Table Columns
        {title:"Planet", field:"pl_name"},
        {title:"System", field:"sys_name"},
        {title:"Planets in System", field:"sy_pnum"},
        {title:"Discovery Year", field:"disc_year"},
        {title:"Star Type", field:"st_spectype"},
        {title:"Distance from Earth", field:"sy_dist"},
        {title:"Number of Stars", field:"sy_snum"},
        {title:"Habitability", field:"habitable"},
      ],
    });

		discRate = new LineChart({
			'parentElement': '#discRate',
			'containerHeight': 300,
			'containerWidth': 500,
      'margin': {top: 15, right: 30, bottom: 50, left: 55},
      'axis' : {x: 'disc_year', y: 'count', xTitle: 'Discovery Year', yTitle: 'Planets Discovered'},
      'position' : {top: 0, left: 0}, 
      'title' : "Discovery Rate", 
		}, this.data); 
    discRate.updateVis(); 

    stars = new barChart({
			'parentElement': '#stars',
			'containerHeight': 300,
			'containerWidth': 500,
      'margin': {top: 15, right: 30, bottom: 35, left: 50},
      'axis' : {x: 'sy_snum', y: 'count', xTitle: 'Number of Stars', yTitle: 'Planets'}, 
      'title' : "Planets with Common Number of Stars",
    }, this.data); 
    stars.updateVis(); 

    starType = new barChart({
			'parentElement': '#starType',
			'containerHeight': 300,
			'containerWidth': 500,
      'margin': {top: 15, right: 30, bottom: 35, left: 50},
      'axis' : {x: 'st_spectype', y: 'count', xTitle: 'Type of Star', yTitle: 'Planets'}, 
      'title' : "Planets with Common Star Type",
    }, this.data); 
    starType.updateVis();  

    systemPlanetNumber = new barChart({
			'parentElement': '#systemPlanetNumber',
			'containerHeight': 300,
			'containerWidth': 500,
      'margin': {top: 15, right: 30, bottom: 35, left: 50},
      'axis' : {x: 'sy_pnum', y: 'count', xTitle: 'Planets in System', yTitle: 'Planets'}, 
      'title' : "Planets with Common System Size",
    }, this.data); 
    systemPlanetNumber.updateVis(); 

    discMethod = new barChart({
			'parentElement': '#discMethod',
			'containerHeight': 300,
			'containerWidth': 1000,
      'margin': {top: 15, right: 30, bottom: 150, left: 50},
      'axis' : {x: 'discoverymethod', y: 'count', xTitle: 'Discovery Method', yTitle: 'Planets Discovered'}, 
      'transform' : 40, 
      'title' : "Discovery Method Count",
    }, this.data); 
    discMethod.updateVis();

    planetSize = new Scatterplot({
			'parentElement': '#planetSize',
			'containerHeight': 300,
			'containerWidth': 500,
      'margin': {top: 20, right: 30, bottom: 50, left: 50},
      'axis' : {x: 'pl_rade', y: 'pl_bmasse', xTitle: 'Radius', yTitle: 'Mass'}, 
      'title' : "Planets Radius and Mass",
    }, this.data); 
    planetSize.updateVis();  

    planetDistance = new Histogram({
			'parentElement': '#planetDistance',
			'containerHeight': 300,
			'containerWidth': 500,
      'margin': {top: 20, right: 30, bottom: 35, left: 50},
      'axis' : {x: 'sy_dist', y: 'length', xTitle: 'Distance from Earth (pc)', yTitle: 'Planets'}, 
      'ticks' : 15,
      'title' : "Planets Distance from Earth",
    }, this.data); 
    planetDistance.updateVis();  

    habitability = new barChart({
			'parentElement': '#habitability',
			'containerHeight': 300,
			'containerWidth': 500,
      'margin': {top: 15, right: 30, bottom: 35, left: 50},
      'axis' : {x: 'habitable', y: 'count', xTitle: 'Habitability', yTitle: 'Planets Discovered'}, 
      'transform' : 0, 
      'title' : "Habitibility Count",
    }, this.data); 
    habitability.updateVis();

  lastClicked = ''; 
  visuals = [discRate, systemPlanetNumber, stars, starType, discMethod, planetSize, planetDistance, habitability]; 
   
})
.catch(error => {
    console.error(error);
});



