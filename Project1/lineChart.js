class LineChart {

    /**
     * Class constructor with basic chart configuration
     * @param {Object}
     * @param {Array}
     */
    constructor(_config, _data) {
      this.config = {
        parentElement: _config.parentElement,
        containerWidth: _config.containerWidth || 800,
        containerHeight: _config.containerHeight || 240,
        margin: _config.margin || {top: 25, right: 30, bottom: 30, left: 50},
        axis: _config.axis,
        title: _config.title,
      }
      this.data = _data;
      this.initVis();
    }
    
    /**
     * Initialize scales/axes and append static chart elements
     */
    initVis() {
      let vis = this;
  
      vis.width = vis.config.containerWidth - vis.config.margin.left - vis.config.margin.right;
      vis.height = vis.config.containerHeight - vis.config.margin.top - vis.config.margin.bottom;
  
      vis.xScale = d3.scaleTime()
          .range([0, vis.width]);
  
      vis.yScale = d3.scaleLinear()
          .range([vis.height, 0])
          .nice();
  
      // Initialize axes
      vis.xAxis = d3.axisBottom(vis.xScale)
          .ticks(6)
          .tickSizeOuter(0)
          .tickPadding(10)
          .tickFormat(d3.format('d')); 
  
      vis.yAxis = d3.axisLeft(vis.yScale)
          .ticks(4)
          .tickSizeOuter(0)
          .tickPadding(10);
  
      // Define size of SVG drawing area
      vis.svg = d3.select(vis.config.parentElement)
          .attr('width', vis.config.containerWidth)
          .attr('height', vis.config.containerHeight);
  
      // Append group element that will contain our actual chart (see margin convention)
      vis.chart = vis.svg.append('g')
          .attr('transform', `translate(${vis.config.margin.left},${vis.config.margin.top})`);
  
      vis.title = vis.chart.append('g')
          .attr('id', 'chart-title')
          .attr('transform', `translate(${vis.width/3}, ${0})`)
          .attr('fill', 'currentColor');  
      
      vis.title.append('text')
          .attr('color', 'currentColor')
          .text(vis.config.title);

      // Append empty x-axis group and move it to the bottom of the chart
      vis.xAxisG = vis.chart.append('g')
          .attr('class', 'axis x-axis')
          .attr('transform', `translate(0,${vis.height})`);
      
      // Append y-axis group
      vis.yAxisG = vis.chart.append('g')
          .attr('class', 'axis y-axis');
  
      // We need to make sure that the tracking area is on top of other chart elements
      vis.marks = vis.chart.append('g');
      vis.trackingArea = vis.chart.append('rect')
          .attr('width', vis.width)
          .attr('height', vis.height)
          .attr('fill', 'none')
          .attr('pointer-events', 'all');
  
      // Empty tooltip group (hidden by default)
      vis.tooltip = vis.chart.append('g')
          .attr('class', 'tooltip')
          .style('display', 'none');
  
      vis.tooltip.append('circle')
          .attr('r', 4);
  
      vis.tooltip.append('text');

      // Append both axis titles
      vis.svg.append('text')
      .attr('class', 'axis-title')
      .attr('y', vis.config.containerHeight - (vis.config.margin.bottom/2))
      .attr('x', vis.config.containerWidth/2)
      .attr('dy', '.71em')
      .attr('fill', 'currentColor')
      .style('text-anchor', 'end')
      .text(`${this.config.axis.xTitle}`);

  vis.svg.append('text')
      .attr('class', 'axis-title')
      .attr('x', -(vis.config.containerHeight/2))
      .attr('y', 0)
      .attr('dy', '.71em')
      .attr('fill', 'currentColor')
      .attr("transform", "rotate(-90)")
      .text(`${this.config.axis.yTitle}`);
    }
  
    /**
     * Prepare the data and scales before we render it.
     */
    updateVis() {
      let vis = this;
      vis.data = getCount(vis.data, vis.config.axis.x); 
      
      vis.xValue = d => d[vis.config.axis.x];
      vis.yValue = d => d[vis.config.axis.y];
  
      vis.line = d3.line()
          .x(d => vis.xScale(vis.xValue(d)))
          .y(d => vis.yScale(vis.yValue(d)));
  
      // Set the scale input domains
      vis.xScale.domain(d3.extent(vis.data, vis.xValue));
      vis.yScale.domain(d3.extent(vis.data, vis.yValue));
  
      vis.bisectDate = d3.bisector(vis.xValue).left;
  
      vis.renderVis();
    }
  
    /**
     * Bind data to visual elements
     */
    renderVis() {
      let vis = this;
  
      // Add line path
      vis.marks.selectAll('.chart-line')
          .data([vis.data])
        .join('path')
          .attr('class', 'chart-line')
          .attr('d', vis.line);
  
      vis.trackingArea
          .on('mouseenter', () => {
            vis.tooltip.style('display', 'block');
          })
          .on('mouseleave', () => {
            vis.tooltip.style('display', 'none');
          })
          .on('mousemove', function(event) {
            // Get date that corresponds to current mouse x-coordinate
            const xPos = d3.pointer(event, this)[0]; // First array element is x, second is y
            const date = vis.xScale.invert(xPos);
  
            // Find nearest data point
            const index = vis.bisectDate(vis.data, date, 1);
            const a = vis.data[index];
            const b = vis.data[index];
            const d = b && (date - a.date > b.date - date) ? b : a; 
  
            // Update tooltip
            vis.tooltip.select('circle')
                .attr('transform', `translate(${vis.xScale(d[vis.config.axis.x])},${vis.yScale(d[vis.config.axis.y])})`);
            
            vis.tooltip.select('text')
                .attr('transform', `translate(${vis.xScale(d[vis.config.axis.x])+ 15},${(vis.yScale(d[vis.config.axis.y]))})`)
                .text(Math.round(d[vis.config.axis.y]));
          });
      
      // Update the axes
      vis.xAxisG.call(vis.xAxis);
      vis.yAxisG.call(vis.yAxis);
    }
  }