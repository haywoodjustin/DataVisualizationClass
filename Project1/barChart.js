class barChart {

    /**
     * Class constructor with basic chart configuration
     * @param {Object}
     * @param {Array}
     */
    constructor(_config, _data) {
      this.config = {
        parentElement: _config.parentElement,
        containerWidth: _config.containerWidth || 600,
        containerHeight: _config.containerHeight || 400,
        margin: _config.margin || {top: 25, right: 20, bottom: 20, left: 35},
        tooltipPadding: _config.tooltipPadding || 15,
        axis: _config.axis,
        transform: _config.transform || 0,
        title: _config.title,  
      }
      this.data = _data;
      this.initVis();
    }

    initVis(){
        let vis = this; 

        vis.width = vis.config.containerWidth - vis.config.margin.left - vis.config.margin.right;
        vis.height = vis.config.containerHeight - vis.config.margin.top - vis.config.margin.bottom;
        
        vis.xScale = d3.scaleBand()
            .range([0, vis.width])
            .paddingInner(0.2)

        vis.yScale = d3.scaleLinear()
            .range([vis.height, 0]);

        // Initialize axes
        vis.xAxis = d3.axisBottom(vis.xScale)
            .tickSizeOuter(0);


        vis.yAxis = d3.axisLeft(vis.yScale);
            // .tickSize(-vis.width - 10);

        // Define size of SVG drawing area
        vis.svg = d3.select(vis.config.parentElement)
            .attr('width', vis.config.containerWidth)
            .attr('height', vis.config.containerHeight); 

        vis.chart = vis.svg.append('g')
            .attr('class', 'Chart')
            .attr('transform', `translate(${vis.config.margin.left},${vis.config.margin.top})`);

        vis.title = vis.chart.append('g')
            .attr('id', 'chart-title')
            .attr('transform', `translate(${vis.width/3}, ${0})`)
            .attr('fill', 'currentColor');  
        
        vis.title.append('text')
            .attr('color', 'currentColor')
            .text(vis.config.title);

        // Append empty x-axis group and move it to the bottom of the chart
        vis.xAxisG = vis.chart.append('g')
            .attr('class', 'axis x-axis')
            .attr('transform', `translate(0,${vis.height})`)
        
        // Append y-axis group
        vis.yAxisG = vis.chart.append('g')
            .attr('class', 'axis y-axis');

        // Append both axis titles
        vis.svg.append('text')
            .attr('class', 'axis-title')
            .attr('y', vis.config.containerHeight - (vis.config.margin.bottom/2))
            .attr('x', vis.config.containerWidth/2)
            .attr('dy', '.71em')
            .attr('fill', 'currentColor')
            .style('text-anchor', 'end')
            .text(`${this.config.axis.xTitle}`);

        vis.svg.append('text')
            .attr('class', 'axis-title')
            .attr('x', -(vis.config.containerHeight/2))
            .attr('y', 0)
            .attr('dy', '.71em')
            .attr("transform", "rotate(-90)")
            .attr("fill", "currentColor")
            .text(`${this.config.axis.yTitle}`);
    }

    updateVis(){
        let vis = this; 
        
        this.data = getCount(this.data, vis.config.axis.x); 

        // Specificy accessor functions
        vis.xValue = d => d[vis.config.axis.x];  
        vis.yValue = d => d[vis.config.axis.y]; 

        vis.xScale.domain(vis.data.map(vis.xValue));   
        vis.yScale.domain([0, d3.max(vis.data, vis.yValue)]);

        if(vis.rectangles) vis.rectangles.remove(); 

        // Add rectangles
        vis.rectangles = vis.chart.selectAll('Rectangle')
            .data(vis.data)
        .join('rect')
            .attr('y', d => vis.yScale(vis.yValue(d)))
            .attr('x', d => vis.xScale(vis.xValue(d)))
            .attr('height', d => (vis.height - vis.yScale(vis.yValue(d))))
            .attr('width', vis.xScale.bandwidth())
            .attr('data', d => d[vis.config.axis.x]);

        vis.xAxisG.call(vis.xAxis)
            .selectAll('text')
            .style('text-anchor', 'start')
            .attr('transform', `rotate(${vis.config.transform})`); 

        vis.rectangles  
            .on('mouseover', (event, d) => {
                d3.select("#tooltip")
                    .style('display', 'block')
                    .style('left', `${event.pageX}px`)
                    .style('top', `${event.pageY}px`)
                    .html(`
                    <div class="tooltip-title">${d[vis.config.axis.x]}</div>
                    <div><i>${d[vis.config.axis.y]} ${this.config.axis.yTitle.toLowerCase()}</i></div>
                    `);
            })
            .on('mousemove', event => {
                d3.select('#tooltip')
                    .style('left', (event.pageX + vis.config.tooltipPadding) + 'px')   
                    .style('top', (event.pageY + vis.config.tooltipPadding) + 'px')
            })
            .on('mouseleave', () => {
                d3.select('#tooltip').style('display', 'none');
              })
            .on('click', (event, d) => {
                dataFilter(d, vis.config.axis.x, vis.config.parentElement); 
            }); 

        vis.xAxisG
            .call(vis.xAxis)
            .call(g => g.select('.domain').remove());

        vis.yAxisG
            .call(vis.yAxis)
            .call(g => g.select('.domain').remove())
    }

}

``