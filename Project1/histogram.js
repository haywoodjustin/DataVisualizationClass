class Histogram {

    /**
     * Class constructor with basic chart configuration
     * @param {Object}
     * @param {Array}
     */
    constructor(_config, _data) {
        // Configuration object with defaults
        this.config = {
            parentElement: _config.parentElement,
            containerWidth: _config.containerWidth || 710,
            containerHeight: _config.containerHeight || 200,
            margin: _config.margin || { top: 10, right: 30, bottom: 30, left: 50},
            tooltipPadding: _config.tooltipPadding || 15,
            axis: _config.axis, 
            title: _config.title,
        }
        this.data = _data;
        this.initVis();
    }

    /**
     * Initialize scales/axes and append static elements, such as axis titles
     */
    initVis() {
        let vis = this;

        // Calculate inner chart size. Margin specifies the space around the actual chart.
        vis.width = vis.config.containerWidth - vis.config.margin.left - vis.config.margin.right;
        vis.height = vis.config.containerHeight - vis.config.margin.top - vis.config.margin.bottom;

        vis.yScale = d3.scaleLinear()
            .range([vis.height, 0])


        vis.xScale = d3.scaleLinear()
            .range([0, vis.width])
            .domain([0, d3.max(vis.data,d => d[vis.config.axis.x])]).nice()

        vis.xAxis = d3.axisBottom(vis.xScale)
            .tickSizeOuter(0);

        vis.yAxis = d3.axisLeft(vis.yScale)
            .tickSizeOuter(0)
            .ticks(6)

        // Define size of SVG drawing area
        vis.svg = d3.select(vis.config.parentElement)
            .attr('width', vis.config.containerWidth)
            .attr('height', vis.config.containerHeight)

        // SVG Group containing the actual chart; D3 margin convention
        vis.chart = vis.svg.append('g')
            .attr('transform', `translate(${vis.config.margin.left},${vis.config.margin.top})`);

        vis.title = vis.chart.append('g')
            .attr('id', 'chart-title')
            .attr('transform', `translate(${vis.width/3}, ${0})`)
            .attr('fill', 'currentColor');  
        
        vis.title.append('text')
            .attr('color', 'currentColor')
            .text(vis.config.title);

        // Append empty x-axis group and move it to the bottom of the chart
        vis.xAxisG = vis.chart.append('g')
            .attr('class', 'axis x-axis')
            .attr('transform', `translate(0,${vis.height})`);

        // Append y-axis group 
        vis.yAxisG = vis.chart.append('g')
            .attr('class', 'axis y-axis');

             // Append both axis titles
        vis.svg.append('text')
        .attr('class', 'axis-title')
        .attr('y', vis.config.containerHeight - (vis.config.margin.bottom/2))
        .attr('x', vis.config.containerWidth/2)
        .attr('dy', '.71em')
        .attr('fill', 'currentColor')
        .style('text-anchor', 'end')
        .text(`${this.config.axis.xTitle}`);

        vis.svg.append('text')
            .attr('class', 'axis-title')
            .attr('x', -(vis.config.containerHeight/2))
            .attr('y', 0)
            .attr('dy', '.71em')
            .attr('fill', 'currentColor')
            .attr("transform", "rotate(-90)")
            .text(`${this.config.axis.yTitle}`);

    }

    /**
     * Prepare data and scales before we render it
     */
    updateVis() {
        let vis = this;

        // set the parameters for the histogram
        vis.histogram = d3.histogram()
            .value(d => d.sy_dist) 
            .domain(vis.xScale.domain()) 
            .thresholds(vis.xScale.ticks(40)); 

        // And apply this function to data to get the bins
        vis.bins = vis.histogram(vis.data);

        vis.yScale.domain([0, d3.max(vis.bins, function(d) {
            return d.length;
        })]).nice();

        vis.renderVis();
    }

    /**
     * Bind data to visual elements
     */
    renderVis() {
        let vis = this;

        if(vis.bars) vis.bars.remove(); 

        // append the bar rectangles to the svg element
        let bars = vis.chart.selectAll("rect")
            .data(vis.bins)
            .enter()
            .append("rect")
            .attr("x", 1)
            .attr("transform", function(d) {
                return "translate(" + vis.xScale(d.x0) + "," + vis.yScale(d.length) + ")";
            })
            .attr("width", function(d) {
                return vis.xScale(d.x1) - vis.xScale(d.x0) - 1;
            })
            .attr("height", function(d) {
                return vis.height - vis.yScale(d.length);
            })

        // Tooltip event listeners
        bars
            .on('mouseover', (event, d) => {
                d3.select('#tooltip')
                    .style('display', 'block')
                    // Format number with million and thousand separator
                    .html(`<div class="tooltip-label">Range</div>${d.x0 + " - " + d.x1 + ' parsecs'} `);
            })
            .on('mousemove', (event) => {
                d3.select('#tooltip')
                    .style('left', (event.pageX + vis.config.tooltipPadding) + 'px')
                    .style('top', (event.pageY + vis.config.tooltipPadding) + 'px')
            })
            .on('mouseleave', () => {
                d3.select('#tooltip')
                .style('display', 'none');
            })

        vis.xAxisG
            .call(vis.xAxis)

        vis.yAxisG
            .call(vis.yAxis);

    }
}