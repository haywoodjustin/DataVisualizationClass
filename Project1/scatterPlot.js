class Scatterplot {

  /**
   * Class constructor with basic chart configuration
   * @param {Object}
   * @param {Array}
   */
  constructor(_config, _data) {
    this.config = {
      parentElement: _config.parentElement,
      containerWidth: _config.containerWidth || 600,
      containerHeight: _config.containerHeight || 400,
      margin: _config.margin || {top: 25, right: 20, bottom: 20, left: 35},
      tooltipPadding: _config.tooltipPadding || 15,
      axis : _config.axis,
      title: _config.title,
    }
    this.data = _data;
    this.initVis();
  }
  
  /**
   * We initialize scales/axes and append static elements, such as axis titles.
   */
  initVis() {
    let vis = this;

    // Calculate inner chart size. Margin specifies the space around the actual chart.
    vis.width = vis.config.containerWidth - vis.config.margin.left - vis.config.margin.right;
    vis.height = vis.config.containerHeight - vis.config.margin.top - vis.config.margin.bottom;

    vis.xScale = d3.scaleLog()
        .range([0, vis.width])
        // .base(10);

    vis.yScale = d3.scaleLog()
        .range([vis.height, 0]);

    // Initialize axes
    vis.xAxis = d3.axisBottom(vis.xScale)
        .ticks(6)
        .tickSize(-vis.height - 10)
        .tickPadding(10); 

    vis.yAxis = d3.axisLeft(vis.yScale)
        .ticks(6)
        .tickSize(-vis.width - 10)
        .tickPadding(10);

    // Define size of SVG drawing area
    vis.svg = d3.select(vis.config.parentElement)
        .attr('width', vis.config.containerWidth)
        .attr('height', vis.config.containerHeight);

    // Append group element that will contain our actual chart 
    // and position it according to the given margin config
    vis.chart = vis.svg.append('g')
        .attr('transform', `translate(${vis.config.margin.left},${vis.config.margin.top})`);

    vis.title = vis.chart.append('g')
        .attr('id', 'chart-title')
        .attr('transform', `translate(${vis.width/3}, ${0})`)
        .attr('fill', 'currentColor');  
    
    vis.title.append('text')
        .attr('color', 'currentColor')
        .text(vis.config.title);
    
    // Append empty x-axis group and move it to the bottom of the chart
    vis.xAxisG = vis.chart.append('g')
        .attr('class', 'axis x-axis')
        .attr('transform', `translate(0,${vis.height})`);
    
    // Append y-axis group
    vis.yAxisG = vis.chart.append('g')
        .attr('class', 'axis y-axis');

    // Append both axis titles
    vis.svg.append('text')
      .attr('class', 'axis-title')
      .attr('y', vis.config.containerHeight - (vis.config.margin.bottom/2))
      .attr('x', vis.config.containerWidth/2)
      .attr('dy', '.71em')
      .attr('fill', 'currentColor')
      .style('text-anchor', 'end')
      .text(`${this.config.axis.xTitle}`);

      vis.svg.append('text')
        .attr('class', 'axis-title')
        .attr('x', -(vis.config.containerHeight/2))
        .attr('y', 0)
        .attr('dy', '.71em')
        .attr('fill', 'currentColor')
        .attr("transform", "rotate(-90)")
        .text(`${this.config.axis.yTitle}`);
  }

  /**
   * Prepare the data and scales before we render it.
   */
  updateVis() {
    let vis = this;
    
    // Specificy accessor functions
    vis.xValue = d => d[vis.config.axis.x];  
    vis.yValue = d => d[vis.config.axis.y];

    // Set the scale input domains
    vis.xScale.domain([.1, d3.max(masterData, vis.xValue)]);
    vis.yScale.domain([.01, d3.max(masterData, vis.yValue)]); 

    vis.renderVis();
  }

  /**
   * Bind data to visual elements.
   */
  renderVis() {
    let vis = this;

    // Add circles
    const circles = vis.chart.selectAll('.point')
        .data(vis.data)
      .join('circle')
        .attr('class', 'point')
        .attr('r', 4)
        .attr('cy', d => vis.yScale(vis.yValue(d)))
        .attr('cx',d => vis.xScale(vis.xValue(d)))
        .attr('fill', 'none')
        .attr('stroke', 'black');

    // Tooltip event listeners
    circles
        .on('mouseover', (event,d) => {
          d3.select('#tooltip')
            .style('display', 'block')
            .style('left', (event.pageX + vis.config.tooltipPadding) + 'px')   
            .style('top', (event.pageY + vis.config.tooltipPadding) + 'px')
            .html(`
            <div class="tooltip-title">${d.pl_name}</div>
            <div>${this.config.axis.xTitle}: ${d[vis.config.axis.x]}</div>
            <div><i>${this.config.axis.yTitle}: ${d[vis.config.axis.y]} </i></div>
            `);
        })
        .on('mouseleave', () => {
          d3.select('#tooltip').style('display', 'none');
        });
    
    // Update the axes/gridlines
    // We use the second .call() to remove the axis and just show gridlines
    vis.xAxisG
        .call(vis.xAxis)
        .call(g => g.select('.domain').remove());

    vis.yAxisG
        .call(vis.yAxis)
        .call(g => g.select('.domain').remove())
  }
}